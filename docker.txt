docker build -t spr.identityserver.web .
docker run -d -p:8001:80 --name spr.identityserver.web --env ASPNETCORE_ENVIRONMENT=Development -e localhost=host.docker.internal vendigo82/spr.identityserver.web

docker run -d -p:8001:80 --name spr.identityserver.web --env ASPNETCORE_ENVIRONMENT=Development --env IdentityServer__BaseUrl="http://host.docker.internal:8000" --env IdentityServer__Authority="http://host.docker.internal:8000" -e localhost=host.docker.internal vendigo82/spr.identityserver.web