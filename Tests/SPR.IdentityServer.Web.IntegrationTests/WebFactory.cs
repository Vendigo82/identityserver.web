﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.IntegrationTests
{
    public class WebFactory : WebApplicationFactory<Startup>
    {
        public readonly Mock<ApiClient.IApiClient> clientMock = new();

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.ConfigureTestServices(services => {
                services
                    .AddAuthentication("Test")
                    .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>("Test", options => { });

                services.RemoveAll<ApiClient.IApiClient>();
                services.AddSingleton(clientMock.Object);

                
                //services.AddAuthorization(options => options.ConfigurePolicies("Test"));

                ////fake IS client
                //services.RemoveAll<IISUserApiClient>();
                //services.AddSingleton<IISUserApiClient, FakeUserApiClient>();

                //services.RemoveAll<IEventsTransport>();
                //services.AddSingleton(EventsMock.Object);

                //services.RemoveAll<Verification.SwaggerQueueClient.IQueueClient>();
                //services.AddSingleton(QueueClientMock.Object);

                //services.RemoveAll<StackExchange.Redis.IDatabase>();
                //services.AddSingleton(RedisDatabaseMock.Object);

                //services.RemoveAll<Verification.AppRepository.IAppRepositoryService>();
                //services.AddSingleton(AppRepositoryMock.Object);

                ////claims factory for tests
                //services.AddSingleton<IClaimsFactory, ClaimsFactory>();
            });
        }

    }

    public class TestAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public TestAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,            
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var principal = new ClaimsPrincipal();
            var ticket = new AuthenticationTicket(principal, "Test");

            var result = AuthenticateResult.Success(ticket);

            return Task.FromResult(result);
        }
    }
}
