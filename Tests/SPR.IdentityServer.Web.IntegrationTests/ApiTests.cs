﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.IntegrationTests
{
    public class ApiTests : IClassFixture<WebFactory>
    {
        private readonly WebFactory fixture;

        public ApiTests(WebFactory fixture)
        {
            this.fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        [Theory, AutoData]
        public async Task HashCodeTest(string secret, string expectedHash)
        {
            // setup
            fixture.clientMock.Setup(f => f.ApiTemplateHashAsync(secret, default))
                .ReturnsAsync(new ApiClient.HashResponse { Hash = expectedHash });

            // action
            var response = await fixture.CreateClient().GetAsync($"api/hash/{secret}");

            // asserts
            response.Should().Be200Ok();
            var body = await response.Content.ReadAsStringAsync();
            body.Should().Be(expectedHash);
        }
    }
}
