﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Pages.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Tests.Pages.Clients
{
    public class ConfigurationModelTests
    {
        private readonly Mock<IClientsSource> sourceMock = new();
        private readonly ConfigurationModel target;

        public ConfigurationModelTests()
        {
            target = new ConfigurationModel(sourceMock.Object);
        }

        [Theory, AutoData]
        public async Task Get_SuccessTest(ClientModel client)
        {
            // setup
            var jo = JObject.Parse("{ }");
            sourceMock.Setup(f => f.GetClientAsync(client.Id)).ReturnsAsync(client);
            sourceMock.Setup(f => f.GetConfigurationAsync(client.Id)).ReturnsAsync(jo);

            // action
            var result = await target.OnGetAsync(client.Id);

            // asserts
            var pageResult = result.Should().BeOfType<PageResult>().Which.Page.Should().BeNull();
            target.InputModel.Should().BeEquivalentTo(new ConfigurationModel.ViewModel {
                ClientId = client.Id,
                ClientName = client.SystemName,
                ConfigurationRaw = jo.ToString()
            });

            sourceMock.Verify(f => f.GetClientAsync(client.Id), Times.Once);
            sourceMock.Verify(f => f.GetConfigurationAsync(client.Id), Times.Once);
            sourceMock.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task Get_NullConfigurationTest(ClientModel client)
        {
            // setup
            sourceMock.Setup(f => f.GetClientAsync(client.Id)).ReturnsAsync(client);
            sourceMock.Setup(f => f.GetConfigurationAsync(client.Id)).ReturnsAsync(JValue.CreateNull());

            // action
            var result = await target.OnGetAsync(client.Id);

            // asserts
            var pageResult = result.Should().BeOfType<PageResult>().Which.Page.Should().BeNull();
            target.InputModel.Should().BeEquivalentTo(new ConfigurationModel.ViewModel {
                ClientId = client.Id,
                ClientName = client.SystemName,
                ConfigurationRaw = null
            });
        }

        [Theory, AutoData]
        public async Task Get_ClientNotFoundTest(Guid clientId)
        {
            // setup
            sourceMock.Setup(f => f.GetClientAsync(clientId)).ReturnsAsync((ClientModel)null);
            sourceMock.Setup(f => f.GetConfigurationAsync(clientId)).ThrowsAsync(new ApiClient.ApiException("", 404, "", null, null));

            // action
            var result = await target.OnGetAsync(clientId);

            // asserts
            var pageResult = result.Should().BeOfType<NotFoundResult>();
        }

        [Theory, AutoData]
        public async Task Post_SuccessTest(Guid clientId)
        {
            // setup
            target.InputModel = new ConfigurationModel.ViewModel() {
                ClientId = clientId,
                ConfigurationRaw = "{ }"
            };

            // action
            var result = await target.OnPostAsync(clientId);

            // asserts
            result.Should().BeOfType<RedirectToPageResult>().Which.PageName.Should().Be("Index");

            sourceMock.Verify(f => f.UpdateConfigurationAsync(clientId, It.IsNotNull<JToken>()), Times.Once);
            sourceMock.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task Post_InvalidJsonTest(Guid clientId)
        {
            // setup
            target.InputModel = new ConfigurationModel.ViewModel() {
                ClientId = clientId,
                ConfigurationRaw = "{ invalid json }"
            };

            // action
            var result = await target.OnPostAsync(clientId);

            // asserts
            result.Should().BeOfType<PageResult>().Which.Page.Should().BeNull();
            target.ModelState.IsValid.Should().BeFalse();
            target.ModelState.Should().ContainSingle().Which.Key.Should()
                .Be($"{nameof(ConfigurationModel.InputModel)}.{nameof(ConfigurationModel.InputModel.ConfigurationRaw)}");

            sourceMock.VerifyNoOtherCalls();
        }
    }
}
