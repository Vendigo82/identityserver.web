﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Clients.Tests
{
    public class IndexTests
    {
        readonly IndexModel pageModel;
        readonly Mock<IClientsSource> clientsMock = new();

        public IndexTests()
        {
            pageModel = new IndexModel(clientsMock.Object);
        }

        [Theory, AutoData]
        public async Task GetTest(IEnumerable<ClientModel> clients)
        {
            // setup
            clientsMock.Setup(f => f.GetClientsAsync()).ReturnsAsync(clients);

            // action
            await pageModel.OnGetAsync();
            
            // asserts
            pageModel.Clients.Should().BeSameAs(clients);
            clientsMock.Verify(f => f.GetClientsAsync(), Times.Once);
        }
    }
}
