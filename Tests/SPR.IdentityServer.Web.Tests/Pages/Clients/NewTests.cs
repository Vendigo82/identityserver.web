﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Clients.Tests
{
    public class NewTests
    {
        readonly Mock<IClientsSource> clientsMock = new();
        readonly NewModel pageModel;

        public NewTests()
        {
            pageModel = new NewModel(clientsMock.Object);
        }

        [Fact]
        public void GetTest()
        {
            // setup

            // action
            pageModel.OnGet();

            // asserts
            pageModel.InputModel.Should().NotBeNull();
            pageModel.InputModel.Id.Should().NotBeEmpty();
        }

        [Theory, AutoData]
        public async Task PostTest(ClientModel input)
        {
            // setup
            pageModel.InputModel = input;
            var args = new List<ClientModel>();
            clientsMock.Setup(f => f.InsertAsync(Capture.In(args))).ReturnsAsync(input.Id);

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            result.Should().BeOfType<RedirectToPageResult>().Which.PageName.Should().Be("Index");
            clientsMock.Verify(f => f.InsertAsync(pageModel.InputModel), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(input);
        }

        [Theory, AutoData]
        public async Task PostInvalidModelTest(ClientModel input)
        {
            // setup
            pageModel.InputModel = input;
            pageModel.ModelState.AddModelError("xx", "xx");

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            result.Should().BeOfType<PageResult>();            
            clientsMock.VerifyNoOtherCalls();
        }
    }
}
