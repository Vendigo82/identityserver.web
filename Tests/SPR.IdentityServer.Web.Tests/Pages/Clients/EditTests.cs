﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Clients.Tests
{
    public class EditTests
    {
        readonly Mock<IClientsSource> clientsMock = new();
        readonly EditModel pageModel;

        public EditTests()
        {
            pageModel = new EditModel(clientsMock.Object);
        }

        [Theory, AutoData]
        public async Task GetTest(ClientModel client)
        {
            // setup
            clientsMock.Setup(f => f.GetClientAsync(client.Id)).ReturnsAsync(client);

            // action
            var result = await pageModel.OnGetAsync(client.Id);

            // asserts
            result.Should().BeOfType<PageResult>();
            pageModel.InputModel.Should().BeSameAs(client);
            clientsMock.Verify(f => f.GetClientAsync(client.Id), Times.Once);
        }

        [Theory, AutoData]
        public async Task GetNotFoundTest(Guid id)
        {
            // setup
            clientsMock.Setup(f => f.GetClientAsync(id)).ReturnsAsync((ClientModel)null);

            // action
            var result = await pageModel.OnGetAsync(id);

            // asserts
            result.Should().BeNotFoundResult();
        }

        [Theory, AutoData]
        public async Task PostTest(ClientModel input)
        {
            // setup
            pageModel.InputModel = input;

            // action
            var result = await pageModel.OnPostAsync(input.Id);

            // asserts
            result.Should().BeOfType<RedirectToPageResult>().Which.PageName.Should().Be("Index");
            clientsMock.Verify(f => f.UpdateAsync(input), Times.Once);
        }

        [Theory, AutoData]
        public async Task PostInvalidModelTest(ClientModel input)
        {
            // setup
            pageModel.InputModel = input;
            pageModel.ModelState.AddModelError("xx", "xx");

            // action
            var result = await pageModel.OnPostAsync(input.Id);

            // asserts
            result.Should().BeOfType<PageResult>();
            clientsMock.VerifyNoOtherCalls();
        }
    }
}
