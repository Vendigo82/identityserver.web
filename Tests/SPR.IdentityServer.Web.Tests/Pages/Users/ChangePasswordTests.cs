﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Users
{
    public class ChangePasswordTests
    {
        private readonly Mock<IUsersSource> usersMock = new();
        private readonly ChangePasswordModel pageModel;

        public ChangePasswordTests()
        {
            var logger = new Mock<ILogger<ChangePasswordModel>>();
            pageModel = new ChangePasswordModel(usersMock.Object, logger.Object);
        }

        [Theory, AutoData]
        public async Task GetTest(UserModel user)
        {
            // setup
            user.LoginType = LoginTypes.Local;
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);

            // action
            var result = await pageModel.OnGetAsync(user.Id);

            // asserts
            result.Should().BeOfType<PageResult>();

            pageModel.Model.Should().BeSameAs(user);
        }

        [Theory, AutoData]
        public async Task GetDomainUserTest(UserModel user)
        {
            // setup
            user.LoginType = LoginTypes.Domain;
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);

            // action
            var result = await pageModel.OnGetAsync(user.Id);

            // asserts
            result.Should().BeStatusCodeResult().WithStatusCode(StatusCodes.Status405MethodNotAllowed);
        }

        [Theory, AutoData]
        public async Task GetUserNotFoundTest(Guid userId)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(userId)).ReturnsAsync((UserModel)null);

            // action
            var result = await pageModel.OnGetAsync(userId);

            // asserts
            result.Should().BeNotFoundResult();
        }

        [Theory, AutoData]
        public async Task PostTest(Guid userId, ChangePasswordModel.InputModel model)
        {
            // setup
            pageModel.Input = model;

            // action
            var result = await pageModel.OnPostAsync(userId);

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().Be("Edit");
            redirect.RouteValues.Should().ContainKey("id").WhoseValue.Should().Be(userId);
            redirect.RouteValues.Should().ContainKey("result").WhoseValue.Should().Be(ResultCodes.PasswordWasChanged);

            usersMock.Verify(f => f.ChangePasswordAsync(userId, model.Password), Times.Once);
        }

        [Theory, AutoData]
        public async Task PostInvalidModelTest(Guid userId, ChangePasswordModel.InputModel model)
        {
            // setup
            pageModel.Input = model;
            pageModel.ModelState.AddModelError("xx", "yy");

            // action
            var result = await pageModel.OnPostAsync(userId);

            // aaserts
            result.Should().BeBadRequestResult();
            usersMock.Verify(f => f.ChangePasswordAsync(userId, model.Password), Times.Never);
        }
    }
}
