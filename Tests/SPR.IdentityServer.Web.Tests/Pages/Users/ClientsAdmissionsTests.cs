﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Users.Tests
{
    public class ClientsAdmissionsTests
    {
        private readonly MockRepository mockRepository = new(MockBehavior.Default);
        private readonly Mock<IUsersSource> usersMock;
        private readonly Mock<IClientsSource> clientsMock;
        private readonly ClientsAdmissionsModel pageModel;

        public ClientsAdmissionsTests()
        {
            usersMock = mockRepository.Create<IUsersSource>();
            clientsMock = mockRepository.Create<IClientsSource>();

            pageModel = new ClientsAdmissionsModel(usersMock.Object, clientsMock.Object);
        }

        [Theory, AutoData]
        public async Task GetTest(IEnumerable<ClientModel> withGrant, IEnumerable<ClientModel> withoutGrant, UserModel user)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);
            clientsMock.Setup(f => f.GetClientsAsync()).ReturnsAsync(withoutGrant.Concat(withGrant));
            usersMock.Setup(f => f.GetClientsAdmissionsAsync(user.Id)).ReturnsAsync(withGrant);

            // action
            var result = await pageModel.OnGetAsync(user.Id);

            // asserts
            result.Should().BeOfType<PageResult>();

            pageModel.UserModel.Should().BeSameAs(user);
            pageModel.Clients.Should().BeEquivalentTo(withoutGrant.Concat(withGrant).Select(i => new { i.Id, i.SystemName }));
            pageModel.Clients.Where(i => i.Selected).Select(i => i.Id).Should().BeEquivalentTo(withGrant.Select(i => i.Id));
            pageModel.Clients.Where(i => !i.Selected).Select(i => i.Id).Should().BeEquivalentTo(withoutGrant.Select(i => i.Id));

            usersMock.Verify(f => f.GetUserAsync(user.Id), Times.Once);
            clientsMock.Verify(f => f.GetClientsAsync(), Times.Once);
            usersMock.Verify(f => f.GetClientsAdmissionsAsync(user.Id), Times.Once);
        }

        [Theory, AutoData]
        public async Task GetUserNotFoundTest(Guid userId)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(userId)).ReturnsAsync((UserModel)null);

            // action
            var result = await pageModel.OnGetAsync(userId);

            // asserts
            result.Should().BeNotFoundResult();
        }

        [Theory, AutoData]
        public async Task PostTest(IList<ClientsAdmissionsModel.ClientSelection> selection, IEnumerable<ClientModel> currentClients, UserModel user)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);
            usersMock.Setup(f => f.GetClientsAdmissionsAsync(user.Id)).ReturnsAsync(currentClients);
            pageModel.Clients = selection;

            // action
            var result = await pageModel.OnPostAsync(user.Id);

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().Be("Edit");
            redirect.RouteValues.Should().ContainKey("id").WhoseValue.Should().Be(user.Id);
            redirect.RouteValues.Should().ContainKey("result").WhoseValue.Should().Be(ResultCodes.ClientsChanged);

            usersMock.Verify(f => f.GetUserAsync(user.Id), Times.Once);
            usersMock.Verify(f => f.GetClientsAdmissionsAsync(user.Id), Times.Once);
            usersMock.Verify(f => f.ChangeClientsAdmissionsAsync(user.Id,
                Match.Create<IEnumerable<Guid>>(i => Enumerable.SequenceEqual(i, selection.Where(i => i.Selected).Select(i => i.Id))),
                Match.Create<IEnumerable<Guid>>(i => Enumerable.SequenceEqual(i, currentClients.Select(i => i.Id)))
                ), Times.Once);
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task PostSelectionNotChangedTest(IList<ClientsAdmissionsModel.ClientSelection> selectionEmpty, IEnumerable<ClientModel> currentClients, UserModel user)
        {
            // setup
            foreach (var s in selectionEmpty)
                s.Selected = false;
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);
            usersMock.Setup(f => f.GetClientsAdmissionsAsync(user.Id)).ReturnsAsync(currentClients);
            pageModel.Clients = selectionEmpty.Concat(currentClients.Select(i => new ClientsAdmissionsModel.ClientSelection { Id = i.Id, Selected = true })).ToList();

            // action
            var result = await pageModel.OnPostAsync(user.Id);

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().Be("Edit");
            redirect.RouteValues.Should().ContainKey("id").WhoseValue.Should().Be(user.Id);
            redirect.RouteValues.Should().ContainKey("result").WhoseValue.Should().Be(ResultCodes.ClientsChanged);

            usersMock.Verify(f => f.GetUserAsync(user.Id), Times.Once);
            usersMock.Verify(f => f.GetClientsAdmissionsAsync(user.Id), Times.Once);
            // without update user's clients
            mockRepository.VerifyNoOtherCalls();
        }

        [Theory, AutoData]
        public async Task InvalidModelTest(UserModel user)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);
            pageModel.ModelState.AddModelError("", "");

            // action
            var result = await pageModel.OnPostAsync(user.Id);

            // asserts
            result.Should().BeBadRequestResult();
        }

        [Theory, AutoData]
        public async Task UserNotFound(Guid userId)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(userId)).ReturnsAsync((UserModel)null);

            // action
            var result = await pageModel.OnPostAsync(userId);

            // asserts
            result.Should().BeNotFoundResult();
        }
    }
}
