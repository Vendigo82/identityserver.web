﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Users.Tests
{
    public class EditTests
    {
        readonly Mock<IUsersSource> usersMock = new();
        readonly EditModel pageModel;

        public EditTests()
        {
            pageModel = new EditModel(usersMock.Object);
        }

        [Theory, AutoData]
        public async Task GetTest(UserModel user)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);

            // action
            var result = await pageModel.OnGetAsync(user.Id, null);

            // asserts
            result.Should().BeOfType<PageResult>();            
            pageModel.InputModel.Should().BeSameAs(user);
            pageModel.ResultCode.Should().BeNull();
            usersMock.Verify(f => f.GetUserAsync(user.Id), Times.Once);
        }

        [Theory, AutoData]
        public async Task GetNotFoundTest(Guid id)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(id)).ReturnsAsync((UserModel)null);

            // action
            var result = await pageModel.OnGetAsync(id, null);

            // asserts
            result.Should().BeNotFoundResult();
        }


        [Theory, AutoData]
        public async Task GetWithResult(string resultCode, UserModel user)
        {
            // setup
            usersMock.Setup(f => f.GetUserAsync(user.Id)).ReturnsAsync(user);

            // action
            var result = await pageModel.OnGetAsync(user.Id, resultCode);

            // asserts
            result.Should().BeOfType<PageResult>();
            pageModel.ResultCode.Should().Be(resultCode);
        }

        [Theory, AutoData]
        public async Task PostTest(UserModel input)
        {
            // setup
            pageModel.InputModel = input;

            // action
            var result = await pageModel.OnPostAsync(input.Id);

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().BeNull();
            redirect.RouteValues.Should().ContainKey("result").WhoseValue.Should().Be(ResultCodes.Edited);
            redirect.RouteValues.Should().ContainKey("id").WhoseValue.Should().Be(input.Id);
            usersMock.Verify(f => f.UpdateAsync(input), Times.Once);
        }

        [Theory, AutoData]
        public async Task PostInvalidModelTest(UserModel input)
        {
            // setup
            pageModel.InputModel = input;
            pageModel.ModelState.AddModelError("xx", "xx");

            // action
            var result = await pageModel.OnPostAsync(input.Id);

            // asserts
            result.Should().BeOfType<PageResult>();
            usersMock.VerifyNoOtherCalls();
        }
    }
}
