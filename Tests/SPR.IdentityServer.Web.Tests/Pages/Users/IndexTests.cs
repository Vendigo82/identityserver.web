﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Users.Tests
{
    public class IndexTests
    {
        private readonly Mock<IUsersSource> sourceMock = new Mock<IUsersSource>();
        private readonly Mock<IOptions<PaginationSettings>> optionsMock = new Mock<IOptions<PaginationSettings>>();
        private readonly IndexModel model;

        public IndexTests()
        {
            optionsMock.SetupGet(f => f.Value).Returns(new PaginationSettings());
            model = new(sourceMock.Object, optionsMock.Object);
        }

        [Theory]
        [InlineAutoData(null)]
        [InlineAutoData("")]
        [InlineAutoData()]
        public async Task GetUsersTest(string filter, IEnumerable<UserModel> users)
        {
            // setup
            model.Filter = filter;
            sourceMock.Setup(f => f.GetCountAsync(filter)).ReturnsAsync(10);
            sourceMock.Setup(f => f.GetListAsync(It.IsAny<int>(), It.IsAny<int>(), filter))
                .ReturnsAsync((users, 10));

            // action
            await model.OnGetAsync(0);

            // asserts
            model.Users.Should().NotBeNull().And.BeSameAs(users);

            sourceMock.Verify(f => f.GetCountAsync(filter), Times.Once);
            sourceMock.Verify(f => f.GetListAsync(It.IsAny<int>(), It.IsAny<int>(), filter), Times.Once);
            sourceMock.VerifyNoOtherCalls();
        }

        public class PaginationTestCase
        {
            public int Page { get; set; }
            public int TotalCount { get; set; }
            public int MaxCount { get; set; }
            public int ConfigPerPage { get; set; }
            public int ExpectedPagesCount { get; set; }
            public int ExpectedPage { get; set; }
        }

        public static IEnumerable<object[]> PaginationTestCases {
            get {
                yield return new object[] { new PaginationTestCase { Page = 0, TotalCount = 20, MaxCount = 10, ConfigPerPage = 5, ExpectedPagesCount = 4, ExpectedPage = 1 } };
                yield return new object[] { new PaginationTestCase { Page = 1, TotalCount = 20, MaxCount = 10, ConfigPerPage = 5, ExpectedPagesCount = 4, ExpectedPage = 1 } };
                yield return new object[] { new PaginationTestCase { Page = 4, TotalCount = 20, MaxCount = 10, ConfigPerPage = 5, ExpectedPagesCount = 4, ExpectedPage = 4 } };
                yield return new object[] { new PaginationTestCase { Page = 1, TotalCount = 20, MaxCount = 10, ConfigPerPage = 20, ExpectedPagesCount = 2, ExpectedPage = 1 } };
                yield return new object[] { new PaginationTestCase { Page = 1, TotalCount = 2, MaxCount = 10, ConfigPerPage = 10, ExpectedPagesCount = 1, ExpectedPage = 1 } };
                yield return new object[] { new PaginationTestCase { Page = 0, TotalCount = 2, MaxCount = 10, ConfigPerPage = 10, ExpectedPagesCount = 1, ExpectedPage = 1 } };
                yield return new object[] { new PaginationTestCase { Page = 1, TotalCount = 0, MaxCount = 10, ConfigPerPage = 10, ExpectedPagesCount = 1, ExpectedPage = 1 } };
            }
        }

        [Theory, MemberData(nameof(PaginationTestCases))]
        public async Task PaginationTest(PaginationTestCase data)
        {
            // setup
            optionsMock.SetupGet(f => f.Value).Returns(new PaginationSettings { ItemsPerPage = data.ConfigPerPage });
            sourceMock.Setup(f => f.GetCountAsync(null)).ReturnsAsync(data.TotalCount);
            sourceMock.Setup(f => f.GetListAsync(It.IsAny<int>(), It.IsAny<int>(), null)).ReturnsAsync((Array.Empty<UserModel>(), data.MaxCount));

            // action
            var result = await model.OnGetAsync(data.Page);

            // asserts
            result.Should().BeOfType<PageResult>();

            model.PageCount.Should().Be(data.ExpectedPagesCount);
            model.PageIndex.Should().Be(data.ExpectedPage);
        }

        public static IEnumerable<object[]> PaginationRedirectTestCases {
            get {
                yield return new object[] { new PaginationTestCase { Page = 3, TotalCount = 10, MaxCount = 5, ConfigPerPage = 5, ExpectedPagesCount = 2, ExpectedPage = 0 }, "xyz" };
                yield return new object[] { new PaginationTestCase { Page = 2, TotalCount = 1, MaxCount = 10, ConfigPerPage = 5, ExpectedPagesCount = 4, ExpectedPage = 0 }, null };
                yield return new object[] { new PaginationTestCase { Page = 2, TotalCount = 0, MaxCount = 10, ConfigPerPage = 5, ExpectedPagesCount = 4, ExpectedPage = 0 }, null };
            }
        }

        [Theory, MemberData(nameof(PaginationRedirectTestCases))]
        public async Task PaginationRedirectTest(PaginationTestCase data, string filter)
        {
            // setup
            model.Filter = filter;
            optionsMock.SetupGet(f => f.Value).Returns(new PaginationSettings { ItemsPerPage = data.ConfigPerPage });
            sourceMock.Setup(f => f.GetCountAsync(null)).ReturnsAsync(data.TotalCount);
            sourceMock.Setup(f => f.GetListAsync(It.IsAny<int>(), It.IsAny<int>(), filter)).ReturnsAsync((Array.Empty<UserModel>(), data.MaxCount));

            // action
            var result = await model.OnGetAsync(data.Page);

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().BeNullOrEmpty();
            redirect.RouteValues.Should().ContainKey("index").WhoseValue.Should().BeNull();
            redirect.RouteValues.Should().ContainKey("filter").WhoseValue.Should().Be(filter);
        }
    }
}
