﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Pages.Users.Tests
{
    public class NewTests
    {
        readonly Mock<IUsersSource> usersMock = new();
        readonly NewModel pageModel;

        public NewTests()
        {
            pageModel = new NewModel(usersMock.Object);
        }

        [Fact]
        public void GetTest()
        {
            // setup

            // action
            pageModel.OnGet();

            // asserts
            pageModel.InputModel.Should().NotBeNull();
            pageModel.InputModel.Id.Should().NotBeEmpty();
            pageModel.InputModel.LoginType.Should().Be(LoginTypes.Local);
        }

        [Theory, AutoData]
        public async Task PostTest(UserModel input)
        {
            // setup
            pageModel.InputModel = input;
            var args = new List<UserModel>();
            usersMock.Setup(f => f.InsertAsync(Capture.In(args))).ReturnsAsync(input.Id);

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            var redirect = result.Should().BeOfType<RedirectToPageResult>().Which;
            redirect.PageName.Should().Be("Edit");
            redirect.RouteValues.Should().ContainKey("id").WhoseValue.Should().Be(input.Id);

            usersMock.Verify(f => f.InsertAsync(pageModel.InputModel), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(input);
        }

        [Theory, AutoData]
        public async Task PostInvalidModelTest(UserModel input)
        {
            // setup
            pageModel.InputModel = input;
            pageModel.ModelState.AddModelError("xx", "xx");

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            result.Should().BeOfType<PageResult>();
            usersMock.VerifyNoOtherCalls();
        }
    }
}
