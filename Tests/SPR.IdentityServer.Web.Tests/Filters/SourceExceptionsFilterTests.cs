﻿using AutoFixture.Xunit2;
using FluentAssertions;
using FluentAssertions.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Filters.Tests
{
    public class SourceExceptionsFilterTests
    {
        readonly Mock<SourceExceptionsFilter> filter;
        readonly ExceptionContext context;

        public SourceExceptionsFilterTests()
        {
            var actionContext = new ActionContext() {
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData(),
                ActionDescriptor = new ActionDescriptor()
            };

            // the List<FilterMetadata> here doesn't have much relevance in the test but is required 
            // for instantiation. So we instantiate a new instance of it with no members to ensure
            // it does not effect the test.
            context = new ExceptionContext(actionContext, new List<IFilterMetadata>());

            var logger = new Mock<ILogger<SourceExceptionsFilter>>();
            filter = new(logger.Object);
        }

        [Theory, AutoData]
        public void ConflictExceptionTest(string key)
        {
            // setup
            var e = new ConflictException("message", null);
            context.Exception = e;
            filter.Protected().Setup<string>("ConflictKey", e).Returns(key);

            // action
            filter.Object.OnException(context);

            // asserts
            context.ExceptionHandled.Should().BeTrue();
            context.Result.Should().NotBeNull().And.BeOfType<PageResult>();
            context.ModelState.Should().ContainSingle().Which.Should().BeEquivalentTo(new { Key = key });
        }

        [Fact]
        public void NotFoundExceptionTest()
        {
            // setup
            var e = new ItemNotFoundException("message", null);
            context.Exception = e;

            // action
            filter.Object.OnException(context);

            // asserts
            context.ExceptionHandled.Should().BeTrue();
            context.Result.Should().BeNotFoundResult();
        }

        [Fact]
        public void OtherExceptionTest()
        {
            // setup
            var e = new Exception(null);
            context.Exception = e;

            // action
            filter.Object.OnException(context);

            // asserts
            context.ExceptionHandled.Should().BeFalse();
            context.Result.Should().BeNull();
        }
    }
}
