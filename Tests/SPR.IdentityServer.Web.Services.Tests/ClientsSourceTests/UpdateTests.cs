﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests
{
    public class UpdateTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientModel model)
        {
            // setup
            var args = new List<ClientDto>();
            clientMock.Setup(f => f.ApiClientsPostAsync(Capture.In(args), default));

            // action
            await source.UpdateAsync(model);

            // asserts
            clientMock.Verify(f => f.ApiClientsPostAsync(It.IsAny<ClientDto>(), default), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(model);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(ClientModel model, ProblemDetails response)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsPostAsync(It.IsAny<ClientDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status404NotFound, null, null, response, null));

            // action
            Func<Task> action = () => source.UpdateAsync(model);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ItemNotFoundException>();
        }

        [Theory, AutoData]
        public async Task ConflictTest(ClientModel model, ProblemDetails response)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsPostAsync(It.IsAny<ClientDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status409Conflict, null, null, response, null));

            // action
            Func<Task> action = () => source.UpdateAsync(model);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ConflictException>();
        }
    }
}
