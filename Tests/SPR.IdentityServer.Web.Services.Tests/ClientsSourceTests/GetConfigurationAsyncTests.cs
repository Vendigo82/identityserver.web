﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests
{
    public class GetConfigurationAsyncTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid clientId)
        {
            // setup
            var response = new JObject();
            clientMock.Setup(f => f.ApiClientsConfigurationGetAsync(clientId, default)).ReturnsAsync(response);

            // action
            var result = await source.GetConfigurationAsync(clientId);

            // asserts
            result.Should().BeSameAs(response);
        }

        [Theory, AutoData]
        public async Task NoContentTest(Guid clientId)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsConfigurationGetAsync(clientId, default))
                .ThrowsAsync(new ApiException("", 204, null, null, null));

            // action
            var result = await source.GetConfigurationAsync(clientId);

            // asserts
            result.Type.Should().Be(JTokenType.Null);
        }
    }
}
