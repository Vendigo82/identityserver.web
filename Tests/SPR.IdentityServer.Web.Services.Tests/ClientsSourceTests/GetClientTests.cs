﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests.Tests
{
    public class GetClientTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientDto client)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsGetAsync(client.Id, default)).ReturnsAsync(client);

            // action
            var result = await source.GetClientAsync(client.Id);

            // asserts
            result.Should().BeEquivalentTo(client);

            clientMock.Verify(f => f.ApiClientsGetAsync(client.Id, default), Times.Once);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(Guid id)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsGetAsync(id, default))
                .ThrowsAsync(new ApiException("", StatusCodes.Status404NotFound, null, null, null));

            // action
            var client = await source.GetClientAsync(id);

            // asserts
            client.Should().BeNull();
        }
    }
}
