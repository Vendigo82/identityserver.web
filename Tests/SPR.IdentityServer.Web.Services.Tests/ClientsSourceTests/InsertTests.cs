﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests
{
    public class InsertTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ClientModel model, ObjectIdDto response)
        {
            // setup
            var args = new List<ClientDto>();
            clientMock.Setup(f => f.ApiClientsPutAsync(Capture.In(args), default)).ReturnsAsync(response);

            // action
            var result = await source.InsertAsync(model);

            // asserts
            result.Should().Be(response.Id);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(model);
            clientMock.Verify(f => f.ApiClientsPutAsync(It.IsAny<ClientDto>(), default), Times.Once);
        }

        [Theory, AutoData]
        public async Task ConflictTest(ClientModel model, ProblemDetails response)
        {
            // setup
            //exception.StatusCode = StatucCodes.
            clientMock.Setup(f => f.ApiClientsPutAsync(It.IsAny<ClientDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status409Conflict, null, null, response, null));

            // action
            Func<Task> action = () => source.InsertAsync(model);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ConflictException>();
        }
    }
}
