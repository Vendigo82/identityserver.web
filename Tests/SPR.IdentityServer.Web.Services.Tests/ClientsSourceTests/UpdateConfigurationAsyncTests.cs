﻿using AutoFixture.Xunit2;
using Moq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests
{
    public class UpdateConfigurationAsyncTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid clientId)
        {
            // setup
            var data = new JObject();

            // action
            await source.UpdateConfigurationAsync(clientId, data);

            // asserts
            clientMock.Verify(f => f.ApiClientsConfigurationPostAsync(clientId, data, default), Times.Once);
        }
    }
}
