﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.ClientsSourceTests
{
    public class GetClientsAsyncTests : ClientsSourceBaseTests
    {
        [Theory, AutoData]
        public async Task GetTest(ClientDtoListContainer response)
        {
            // setup
            clientMock.Setup(f => f.ApiClientsGetAsync(default)).ReturnsAsync(response);

            // action
            var result = await source.GetClientsAsync();

            // asserts
            result.Should().BeEquivalentTo(response.Items);
        }
    }
}
