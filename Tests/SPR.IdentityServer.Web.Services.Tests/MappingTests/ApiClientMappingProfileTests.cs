﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.MappingTests
{
    public class ApiClientMappingProfileTests
    {
        [Fact]
        public void ValidateConfigurationTest()
        {
            // setup
            var config = new MapperConfiguration(c => {
                c.AddProfile<Mapping.ApiClientMappingProfile>();
                c.EnableEnumMappingValidation();
            });

            config.AssertConfigurationIsValid();
        }
    }
}
