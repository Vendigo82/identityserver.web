﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class GetClientsAdmissionsTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid userId, ClientDtoListContainer response)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersClientsGetAsync(userId, default)).ReturnsAsync(response);

            // action
            var result = await source.GetClientsAdmissionsAsync(userId);

            // asserts
            result.Should().BeEquivalentTo(response.Items);
            clientMock.Verify(f => f.ApiUsersClientsGetAsync(userId, default), Times.Once);
        }
    }
}
