﻿using AutoMapper;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class UsersSourceBaseTests
    {
        protected readonly Mock<IApiClient> clientMock = new ();        
        protected readonly UsersSource source; 

        public UsersSourceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<Mapping.ApiClientMappingProfile>()));
            source = new(clientMock.Object, mapper);
        }
    }
}
