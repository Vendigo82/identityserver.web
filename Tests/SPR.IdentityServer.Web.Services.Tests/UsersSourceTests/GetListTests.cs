﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class GetListTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(string filter, int count, int offset, ApiClient.UserDtoPartialListContainer response)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersGetAsync(offset, count, filter, default)).ReturnsAsync(response);

            // action
            var result = await source.GetListAsync(count, offset, filter);

            // asserts
            result.List.Should().BeEquivalentTo(response.Items);
            result.MaxCount.Should().Be(response.MaxCount);

            clientMock.Verify(f => f.ApiUsersGetAsync(offset, count, filter, default), Times.Once);
        }
    }
}
