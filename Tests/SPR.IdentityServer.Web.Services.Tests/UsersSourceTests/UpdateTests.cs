﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class UpdateTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserModel user)
        {
            // setup
            var args = new List<UserDto>();
            clientMock.Setup(f => f.ApiUsersPostAsync(Capture.In(args), default));

            // action
            await source.UpdateAsync(user);

            // asserts
            clientMock.Verify(f => f.ApiUsersPostAsync(It.IsAny<UserDto>(), default), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(user);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(UserModel user)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersPostAsync(It.IsAny<UserDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status404NotFound, "", null, null, null));

            // action
            Func<Task> action = () => source.UpdateAsync(user);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ItemNotFoundException>();
        }

        [Theory, AutoData]
        public async Task ConflictTest(UserModel user)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersPostAsync(It.IsAny<UserDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status409Conflict, "", null, null, null));

            // action
            Func<Task> action = () => source.UpdateAsync(user);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ConflictException>();
        }
    }
}
