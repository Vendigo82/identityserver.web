﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class InsertTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserModel user, ObjectIdDto response)
        {
            // setup
            var args = new List<UserDto>();
            clientMock.Setup(f => f.ApiUsersPutAsync(Capture.In(args), default)).ReturnsAsync(response);

            // action
            var result = await source.InsertAsync(user);

            // asserts
            result.Should().Be(response.Id);

            clientMock.Verify(f => f.ApiUsersPutAsync(It.IsAny<UserDto>(), default), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(user);
        }

        [Theory, AutoData]
        public async Task ConflictTest(UserModel user)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersPutAsync(It.IsAny<UserDto>(), default))
                .ThrowsAsync(new ApiException<ProblemDetails>("", StatusCodes.Status409Conflict, "", null, null, null));

            // action
            Func<Task> action = () => source.InsertAsync(user);

            // asserts
            await action.Should().ThrowExactlyAsync<ConflictException>();
        }
    }
}
