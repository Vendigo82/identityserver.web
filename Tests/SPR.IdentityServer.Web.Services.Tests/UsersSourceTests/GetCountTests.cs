﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class GetCountTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(string filter, CountDto response)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersCountAsync(filter, default)).ReturnsAsync(response);

            // action
            var result = await source.GetCountAsync(filter);

            // asserts
            result.Should().Be(response.Count);
            clientMock.Verify(f => f.ApiUsersCountAsync(filter, default), Times.Once);
        }
    }
}
