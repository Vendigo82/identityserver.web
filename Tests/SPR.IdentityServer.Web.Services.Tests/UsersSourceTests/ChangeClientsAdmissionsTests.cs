﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class ChangeClientsAdmissionsTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid userId, IEnumerable<Guid> grant, IEnumerable<Guid> revoke)
        {
            // setup
            var args = new List<UpdateUserClientsRequest>();
            clientMock.Setup(f => f.ApiUsersClientsPostAsync(userId, Capture.In(args), default));

            // action
            await source.ChangeClientsAdmissionsAsync(userId, grant, revoke);

            // asserts
            clientMock.Verify(f => f.ApiUsersClientsPostAsync(userId, It.IsAny<UpdateUserClientsRequest>(), default), Times.Once);
            args.Should().ContainSingle().Which.Should().BeEquivalentTo(new {
                Grant = grant,
                Revoke = revoke
            });
        }
    }
}
