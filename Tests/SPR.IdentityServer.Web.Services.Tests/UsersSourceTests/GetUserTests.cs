﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.IdentityServer.ApiClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class GetUserTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto user)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersGetAsync(user.Id, default)).ReturnsAsync(user);

            // action
            var result = await source.GetUserAsync(user.Id);

            // asserts
            result.Should().NotBeNull().And.BeEquivalentTo(user, o => o.ComparingEnumsByName());
            clientMock.Verify(f => f.ApiUsersGetAsync(user.Id, default), Times.Once);
        }

        [Theory, AutoData]
        public async Task NotFoundTest(Guid id)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersGetAsync(id, default)).ThrowsAsync(new ApiException("", 404, "", null, null));

            // action
            var result = await source.GetUserAsync(id);

            // asserts
            result.Should().BeNull();
        }
    }
}
