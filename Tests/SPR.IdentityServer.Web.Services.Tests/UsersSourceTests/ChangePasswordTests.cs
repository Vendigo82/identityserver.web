﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.IdentityServer.Web.Services.UsersSourceTests
{
    public class ChangePasswordTests : UsersSourceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(Guid userId, string newPassword)
        {
            // setup

            // action
            await source.ChangePasswordAsync(userId, newPassword);

            // asserts
            clientMock.Verify(f => f.ApiUsersPasswordAsync(userId,
                Match.Create<ApiClient.ChangePasswordRequest>(i => i.Password == newPassword), default), Times.Once);
        }

        [Theory, AutoData]
        public async Task UserNotFoundTest(Guid userId, string newPassword)
        {
            // setup
            clientMock.Setup(f => f.ApiUsersPasswordAsync(userId, It.IsAny<ApiClient.ChangePasswordRequest>(), default))
                .ThrowsAsync(new ApiClient.ApiException("", StatusCodes.Status404NotFound, null, null, null));

            // action
            Func<Task> action = () => source.ChangePasswordAsync(userId, newPassword);

            // asserts
            await action.Should().ThrowExactlyAsync<Exceptions.ItemNotFoundException>();
        }
    }
}
