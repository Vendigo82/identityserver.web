﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Exceptions
{
    public class ItemNotFoundException : Exception
    {
        public ItemNotFoundException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
