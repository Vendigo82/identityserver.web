﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.IdentityServer.Web.DataModel
{
    public class UserModel
    {
        public Guid Id { get; set; }

        [Required]
        [MinLength(2)]
        public string Login { get; set; } = null!;

        [Required]
        public LoginTypes LoginType { get; set; }

        public bool IsDisabled { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; } = null!;
    }
}
