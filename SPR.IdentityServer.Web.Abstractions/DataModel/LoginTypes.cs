﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.DataModel
{
    /// <summary>
    /// Type of user's login
    /// </summary>
    public enum LoginTypes
    {
        /// <summary>
        /// Local user
        /// </summary>
        Local,

        /// <summary>
        /// Domain user
        /// </summary>
        Domain,
    }
}
