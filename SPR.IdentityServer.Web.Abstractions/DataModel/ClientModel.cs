﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.IdentityServer.Web.DataModel
{
    public class ClientModel
    {
        public Guid Id { get; set; }

        [Required]
        [MinLength(3)]
        public string SystemName { get; set; } = null!;

        public bool IsDisabled { get; set; }

        [Required]
        [MinLength(3)]
        public string Title { get; set; } = null!;

        [Required]
        public bool RequirePermission { get; set; } = true;
    }
}
