﻿using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Abstractions
{
    /// <summary>
    /// Provide operations to work with users
    /// </summary>
    public interface IUsersSource
    {
        /// <summary>
        /// Get list of users
        /// </summary>
        /// <param name="count">count users in list</param>
        /// <param name="offset">offset in list</param>
        /// <returns></returns>
        Task<(IEnumerable<UserModel> List, int MaxCount)> GetListAsync(int count, int offset, string? filter);

        /// <summary>
        /// Get Count of users with filter
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        Task<int> GetCountAsync(string? filter);

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>User or null if not found</returns>
        Task<UserModel?> GetUserAsync(Guid id);

        /// <summary>
        /// Inser new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.ConflictException"></exception>
        Task<Guid> InsertAsync(UserModel user);

        /// <summary>
        /// Update existed user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="Exceptions.ConflictException"></exception>
        /// <exception cref="Exceptions.ItemNotFoundException"></exception>
        Task UpdateAsync(UserModel model);

        /// <summary>
        /// Change user's password
        /// </summary>
        /// <param name="password">new password</param>
        /// <returns></returns>
        Task ChangePasswordAsync(Guid userId, string password);

        /// <summary>
        /// Get list of clients to which user has admission
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<ClientModel>> GetClientsAdmissionsAsync(Guid userId);

        /// <summary>
        /// Update list of clients
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="grant">grant admisson</param>
        /// <param name="revoke">revoke admission</param>
        /// <returns></returns>
        Task ChangeClientsAdmissionsAsync(Guid userId, IEnumerable<Guid> grant, IEnumerable<Guid> revoke);
    }
}
