﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Abstractions
{
    public interface IApiService
    {
        public Task<string> HashSecretAsync(string secret);
    }
}
