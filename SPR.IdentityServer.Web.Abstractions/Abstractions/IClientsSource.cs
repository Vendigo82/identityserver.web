﻿using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Abstractions
{
    /// <summary>
    /// Provide access to clients
    /// </summary>
    public interface IClientsSource
    {
        /// <summary>
        /// Get list of clients
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<ClientModel>> GetClientsAsync();

        /// <summary>
        /// Get client by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Client or null</returns>
        public Task<ClientModel?> GetClientAsync(Guid id);

        /// <summary>
        /// Update existed item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Task UpdateAsync(ClientModel model);

        /// <summary>
        /// Inserts new item
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Task<Guid> InsertAsync(ClientModel model);

        /// <summary>
        /// Load client's configuration
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<JToken> GetConfigurationAsync(Guid id);

        /// <summary>
        /// Update client's configuration
        /// </summary>
        /// <param name="id"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public Task UpdateConfigurationAsync(Guid id, JToken configuration);
    }
}
