using HealthChecks.UI.Client;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.Services;
using SPR.IdentityServer.Web.Services.LogWrappers;
using SPR.IdentityServer.Web.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment host)
        {
            Configuration = configuration;
            Host = host;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment Host { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllers();

            services.Configure<PaginationSettings>(Configuration.GetSection("UsersPage"));
            var identityServerSettings = Configuration.GetSection("IdentityServer").Get<IdentityServerSettings>();

            services.AddProblemDetails();

            AddIdentityServer();
            AddHealthChecks();

            services.AddAccessTokenManagement();
            services.AddDefaultConfigurationHttpClientPolicies(Configuration);

            //services.AddCorrelationId();
            //services.AddHeaderPropagation(options => { options.Headers.Add(Correlation.Header); });

            services
                .AddHttpClient<IApiClient, ApiClient.ApiClient>(client => {
                    client.BaseAddress = identityServerSettings.BaseUrl;
                })
                .AddUserAccessTokenHandler()
                .AddHeaderPropagation()
                .AddLoggingHandler()
                .AddConfigurationPolicies();                

            services.AddTransient<ClientsSource>();
            services.AddTransient<UsersSource>();
            services.AddTransient<IClientsSource, ClientsSourceLogWrapper<ClientsSource>>();
            services.AddTransient<IUsersSource, UsersSourceLogWrapper<UsersSource>>();
            services.AddTransient<IApiService, ApiService>();
            services.AddAutoMapper(typeof(Services.Mapping.ApiClientMappingProfile));

            services.ConfigureNonBreakingSameSiteCookies();

            void AddIdentityServer()
            {
                services
                    .AddAuthentication(options => {
                        options.DefaultScheme = "Cookies";
                        options.DefaultChallengeScheme = "oidc";
                    })
                    .AddCookie("Cookies", options => {
                        options.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                        options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None;
                    })
                    .AddOpenIdConnect("oidc", options => {
                        options.Authority = identityServerSettings.Authority;
                        options.RequireHttpsMetadata = identityServerSettings.RequireHttpsMetadata;

                        options.ClientId = "is_admin";
                        options.ClientSecret = identityServerSettings.ClientSecret ?? "miceeatingrice";
                        options.ResponseType = "code";

                        options.SaveTokens = true;
                        options.UseTokenLifetime = true;

                        options.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(0);

                        options.Scope.Add("openid");
                        options.Scope.Add("profile");
                        options.Scope.Add("offline_access");
                        options.Scope.Add("users_api.read");
                        options.Scope.Add("users_api.write_clients");
                        options.Scope.Add("users_api.write_users");
                        options.Scope.Add("users_api.change_password");
                    });

            }

            void AddHealthChecks()
            {
                services
                    .AddHealthChecks()
                    .AddIdentityServer(identityServerSettings.BaseUrl)
                    .AddUrlGroup(new Uri(identityServerSettings.BaseUrl, "health"), "users-api");
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //app.UseCorrelationId();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseWhen(c => c.Request.Path.StartsWithSegments("/api"), b => b.UseProblemDetails());

            app.UseHealthChecks("/health", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseHeaderPropagation();

            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapRazorPages().RequireAuthorization();
                endpoints.MapControllers().RequireAuthorization();
            });
        }
    }
}
