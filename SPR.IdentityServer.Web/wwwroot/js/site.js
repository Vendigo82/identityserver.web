﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function hashSecret() {    
    var secret = $('#secret-input').val();
    $.ajax({        
        type: "GET",
        url: "/api/hash/" + secret,
        complete: function (data) {            
            $('#secret-hash-input').val(data.responseText);
        }, error: function (data) {
            console.log(data);
        }
    });
}