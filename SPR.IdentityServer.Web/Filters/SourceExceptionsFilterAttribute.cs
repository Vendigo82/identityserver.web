﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.Exceptions;
using System;

namespace SPR.IdentityServer.Web.Filters
{
    public abstract class SourceExceptionsFilter : IExceptionFilter
    {
        private readonly ILogger<SourceExceptionsFilter> _logger;

        public SourceExceptionsFilter(ILogger<SourceExceptionsFilter> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void OnException(ExceptionContext context)
        {
            var result = ThreatException();
            if (result != null) {
                _logger.LogInformation(context.Exception, "Api exception");
                context.Result = result;
                context.ExceptionHandled = true;
            }

            IActionResult? ThreatException()
            {
                switch (context.Exception) {
                    case ConflictException conflict:
                        context.ModelState.AddModelError(ConflictKey(conflict), conflict.Message);
                        return new PageResult();

                    case ItemNotFoundException:
                        return new NotFoundResult();

                    default:
                        return null;
                }
            }
        }

        protected abstract string ConflictKey(ConflictException exception);
    }
}
