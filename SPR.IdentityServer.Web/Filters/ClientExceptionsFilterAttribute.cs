﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;

namespace SPR.IdentityServer.Web.Filters
{
    public class ClientExceptionsFilter : SourceExceptionsFilter
    {
        public ClientExceptionsFilter(ILogger<ClientExceptionsFilter> logger) : base(logger)
        {            
        }

        protected override string ConflictKey(ConflictException exception) => nameof(ClientModel.SystemName);
    }

    public class ClientExceptionsFilterAttribute : TypeFilterAttribute
    {
        public ClientExceptionsFilterAttribute() : base(typeof(ClientExceptionsFilter))
        {
        }
    }
}
