﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;

namespace SPR.IdentityServer.Web.Filters
{
    public class UserExceptionsFilter : SourceExceptionsFilter
    {
        public UserExceptionsFilter(ILogger<UserExceptionsFilter> logger) : base(logger)
        {
        }

        protected override string ConflictKey(ConflictException exception) => nameof(UserModel.Login);
    }

    public class UserExceptionsFilterAttribute : TypeFilterAttribute
    {
        public UserExceptionsFilterAttribute() : base(typeof(UserExceptionsFilter))
        {
        }
    }
}
