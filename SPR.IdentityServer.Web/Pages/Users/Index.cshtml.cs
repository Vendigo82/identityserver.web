using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Settings;

namespace SPR.IdentityServer.Web.Pages.Users
{
    public class IndexModel : PageModel
    {
        private readonly IUsersSource _source;
        private readonly IOptions<PaginationSettings> _options;

        public IndexModel(IUsersSource source, IOptions<PaginationSettings> options)
        {
            _source = source ?? throw new ArgumentNullException(nameof(source));
            _options = options ?? throw new ArgumentNullException(nameof(options));
        }

        public IEnumerable<UserModel> Users { get; private set; } = null!;

        public int PageIndex { get; private set; }

        public int PageCount { get; private set; }

        [BindProperty(Name = "filter", SupportsGet = true)]
        public string? Filter { get; set; }

        public async Task<IActionResult> OnGetAsync([FromRoute] int index)
        {
            if (!ModelState.IsValid)
                return Page ();

            index = Math.Max(0, index - 1);

            int offer = index * _options.Value.ItemsPerPage;
            var taskUsers = _source.GetListAsync(_options.Value.ItemsPerPage, offer, Filter);
            var taskCount = _source.GetCountAsync(Filter);

            await Task.WhenAll(taskUsers, taskCount);

            Users = taskUsers.Result.List;

            var count = Math.Min(_options.Value.ItemsPerPage, taskUsers.Result.MaxCount);            
            PageIndex = count != 0 ? offer / count + 1 : 1;
            PageCount = Math.Max(1, count != 0 ? taskCount.Result / count : 0);

            if (PageIndex > PageCount)
                return RedirectToPage(new { index = (int?)null, filter = Filter });
            else
                return Page();
        }
    }
}
