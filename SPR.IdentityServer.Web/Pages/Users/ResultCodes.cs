﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Pages.Users
{
    public static class ResultCodes
    {
        public const string PasswordWasChanged = "pswok";
        public const string Edited = "ok";
        public const string ClientsChanged = "clients";
    }
}
