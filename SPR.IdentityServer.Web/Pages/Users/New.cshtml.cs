using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Users
{
    [UserExceptionsFilter]
    public class NewModel : PageModel
    {
        private readonly IUsersSource _users;

        public NewModel(IUsersSource users)
        {
            _users = users ?? throw new ArgumentNullException(nameof(users));
        }

        [BindProperty]
        public UserModel InputModel { get; set; } = null!;

        public void OnGet()
        {
            InputModel = new UserModel() { Id = Guid.NewGuid() };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            var id = await _users.InsertAsync(InputModel);

            return RedirectToPage("Edit", new { id });
        }
    }
}
