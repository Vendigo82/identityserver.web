using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Users
{
    [UserExceptionsFilter]
    public class EditModel : PageModel
    {
        private readonly IUsersSource _users;

        public EditModel(IUsersSource users)
        {
            _users = users ?? throw new ArgumentNullException(nameof(users));
        }

        [BindProperty]
        public UserModel InputModel { get; set; } = null!;

        public string? ResultCode { get; private set; }

        public async Task<IActionResult> OnGetAsync(Guid id, [FromQuery] string? result)
        {
            var item = await _users.GetUserAsync(id);
            if (item == null)
                return NotFound();

            InputModel = item;
            ResultCode = result;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
                return Page();

            await _users.UpdateAsync(InputModel);

            return RedirectToPage(new { id, result = ResultCodes.Edited });
        }
    }
}
