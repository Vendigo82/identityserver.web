using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;

namespace SPR.IdentityServer.Web.Pages.Users
{
    public class ClientsAdmissionsModel : PageModel
    {
        private readonly IUsersSource _users;
        private readonly IClientsSource _clients;

        public ClientsAdmissionsModel(IUsersSource users, IClientsSource clients)
        {
            _users = users ?? throw new ArgumentNullException(nameof(users));
            _clients = clients ?? throw new ArgumentNullException(nameof(clients));
        }

        public UserModel UserModel { get; set; } = null!;

        [BindProperty]
        public IList<ClientSelection> Clients { get; set; } = null!;

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            var user = await _users.GetUserAsync(id);
            if (user == null)
                return NotFound();

            UserModel = user;
            var admissions = (await _users.GetClientsAdmissionsAsync(id)).Select(i => i.Id).ToHashSet();
            var clients = await _clients.GetClientsAsync();

            Clients = clients.Select(i => new ClientSelection { 
                Id = i.Id, 
                SystemName = i.SystemName, 
                Selected = admissions.Contains(i.Id) 
            }).ToList();            

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            var user = await _users.GetUserAsync(id);
            if (user == null)
                return NotFound();

            UserModel = user;
            if (!ModelState.IsValid)
                return BadRequest();

            var admissions = (await _users.GetClientsAdmissionsAsync(id)).Select(i => i.Id).ToArray();
            var revoke = admissions.Except(Clients.Where(i => i.Selected).Select(i => i.Id)).ToArray();
            var grant = Clients.Where(i => i.Selected).Select(i => i.Id).Except(admissions).ToArray();

            if (revoke.Any() || grant.Any())
                await _users.ChangeClientsAdmissionsAsync(id, grant, revoke);

            return RedirectToPage("Edit", new { id, result = ResultCodes.ClientsChanged });
        }

        public class ClientSelection
        {
            public bool Selected { get; set; }

            public Guid Id { get; set; }

            public string SystemName { get; set; } = null!;
        }
    }
}
