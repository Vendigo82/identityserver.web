using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Users
{
    [UserExceptionsFilter]
    public class ChangePasswordModel : PageModel
    {
        private readonly IUsersSource _source;
        private readonly ILogger<ChangePasswordModel> _logger;

        public ChangePasswordModel(IUsersSource source, ILogger<ChangePasswordModel> logger)
        {
            _source = source ?? throw new ArgumentNullException(nameof(source));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public UserModel Model { get; private set; } = null!;

        [BindProperty]
        public InputModel Input { get; set; } = null!;

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            var user = await _source.GetUserAsync(id);
            if (user == null)
                return NotFound();

            Model = user;
            if (user.LoginType != LoginTypes.Local) {
                _logger.LogWarning(TryChangePasswordMsg, id, user.LoginType);
                return StatusCode(StatusCodes.Status405MethodNotAllowed);
            }            

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _source.ChangePasswordAsync(id, Input.Password);

            return RedirectToPage("Edit", new { id, result = ResultCodes.PasswordWasChanged });
        }

        public class InputModel
        {
            [Required]
            [MinLength(3)]
            public string Password { get; set; } = null!;

            [Required]
            [Compare(nameof(Password))]
            public string RepeatPassword { get; set; } = null!;
        }

        private const string TryChangePasswordMsg = "Try to change password for user {" + LogValues.UserId + "} with login type {" + LogValues.LoginType + "}";
    }
}
