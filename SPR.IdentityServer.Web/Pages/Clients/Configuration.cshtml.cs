using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Clients
{
    [ClientExceptionsFilter]
    public class ConfigurationModel : PageModel
    {
        private readonly IClientsSource _source;

        public ConfigurationModel(IClientsSource source)
        {
            _source = source ?? throw new ArgumentNullException(nameof(source));
        }

        [BindProperty]
        public ViewModel InputModel { get; set; } = null!;

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            var clientTask = _source.GetClientAsync(id);
            var configTask = _source.GetConfigurationAsync(id);
            //await Task.WhenAll(clientTask, configTask);

            await clientTask;
            if (clientTask.Result == null)
                return NotFound();

            await configTask;

            InputModel = new ViewModel() {
                ClientId = clientTask.Result.Id,
                ClientName = clientTask.Result.SystemName
            };

            if (configTask.Result.Type != JTokenType.Null)
                InputModel.ConfigurationRaw = configTask.Result.ToString();

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
                return Page();

            JToken? token;
            try
            {
                token = JToken.Parse(InputModel.ConfigurationRaw!);
            } 
            catch (JsonException e)
            {
                ModelState.AddModelError($"{nameof(InputModel)}.{nameof(InputModel.ConfigurationRaw)}", $"Invalid json format: {e.Message}");
                return Page();
            }

            await _source.UpdateConfigurationAsync(id, token);

            return RedirectToPage("Index");
        }

        public class ViewModel
        {
            public Guid ClientId { get; set; }

            public string ClientName { get; set; } = null!;

            [Required]
            public string? ConfigurationRaw { get; set; }
        }
    }
}
