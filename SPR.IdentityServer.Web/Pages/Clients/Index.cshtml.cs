using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;

namespace SPR.IdentityServer.Web.Pages.Clients
{
    public class IndexModel : PageModel
    {
        private readonly IClientsSource _clients;

        public IndexModel(IClientsSource clients)
        {
            _clients = clients ?? throw new ArgumentNullException(nameof(clients));
        }

        public IEnumerable<ClientModel> Clients { get; private set; } = null!;

        public async Task OnGetAsync()
        {
            Clients = await _clients.GetClientsAsync();
        }
    }
}
