using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Clients
{
    [ClientExceptionsFilter]
    public class NewModel : PageModel
    {
        private readonly IClientsSource _clients;

        public NewModel(IClientsSource clients)
        {
            _clients = clients ?? throw new ArgumentNullException(nameof(clients));
        }

        [BindProperty]
        public ClientModel InputModel { get; set; } = null!;

        public void OnGet()
        {
            InputModel = new ClientModel() { Id = Guid.NewGuid() };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            var id = await _clients.InsertAsync(InputModel);

            return RedirectToPage("Index");
        }
    }
}
