using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Filters;

namespace SPR.IdentityServer.Web.Pages.Clients
{
    [ClientExceptionsFilter]
    public class EditModel : PageModel
    {
        private readonly IClientsSource _clients;

        public EditModel(IClientsSource clients)
        {
            _clients = clients ?? throw new ArgumentNullException(nameof(clients));
        }

        [BindProperty]
        public ClientModel InputModel { get; set; } = null!;

        public async Task<IActionResult> OnGetAsync(Guid id)
        {
            // there are no method for request single item
            var item = await _clients.GetClientAsync(id);
            if (item == null)
                return NotFound();

            InputModel = item;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(Guid id)
        {
            if (!ModelState.IsValid)
                return Page();

            await _clients.UpdateAsync(InputModel);
            return RedirectToPage("Index");
        }
    }
}
