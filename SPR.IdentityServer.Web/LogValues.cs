﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web
{
    public static class LogValues
    {
        public const string UserId = "UserId";
        public const string LoginType = "LoginType";
    }
}
