﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SPR.IdentityServer.Web.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Controllers
{
    [Route("api")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        private readonly IApiService _source;

        public ApiController(IApiService source)
        {
            _source = source ?? throw new ArgumentNullException(nameof(source));
        }

        [HttpGet("[action]/{secret}")]
        [AllowAnonymous]
        public async Task<IActionResult> HashAsync(string secret)
        {
            var hash = await _source.HashSecretAsync(secret);
            return Ok(hash);
        }

        [HttpGet("[action]")]
        [AllowAnonymous]
        public IActionResult Version()
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version?.ToString();
            return Ok(new { Version = version });
        }
    }
}
