﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Settings
{
    public class IdentityServerSettings
    {
        [Required]
        public Uri BaseUrl { get; set; } = null!;

        [Required]
        public string Authority { get; set; } = null!;

        public bool RequireHttpsMetadata { get; set; } = true;

        public string? ClientSecret { get; set; }
    }
}
