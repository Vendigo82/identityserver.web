﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Settings
{
    public class PaginationSettings
    {
        [Range(1, 100)]
        public int ItemsPerPage { get; set; } = 20;
    }
}
