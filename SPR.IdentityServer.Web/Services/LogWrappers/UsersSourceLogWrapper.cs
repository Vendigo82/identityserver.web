﻿using Microsoft.Extensions.Logging;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services.LogWrappers
{
    public class UsersSourceLogWrapper<T> : IUsersSource where T : IUsersSource
    {
        private readonly T _origin;
        private readonly ILogger<T> _logger;

        public UsersSourceLogWrapper(T origin, ILogger<T> logger)
        {
            _origin = origin;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task ChangeClientsAdmissionsAsync(Guid userId, IEnumerable<Guid> grant, IEnumerable<Guid> revoke)
        {
            const string logBefore = "Changing user's {" + LogValues.UserId +"} admissions for clients: grant {grant}, revoke {revoke}";
            const string logAfter = "User's {" + LogValues.UserId + "} admissions for clients was changed. Granted {grant}, Revoked {revoke}";

            _logger.LogDebug(logBefore, userId, grant, revoke);
            await _origin.ChangeClientsAdmissionsAsync(userId, grant, revoke);
            _logger.LogInformation(logAfter, userId, grant, revoke);
        }

        public async Task ChangePasswordAsync(Guid userId, string password)
        {
            const string logBefore = "Changing user's {" + LogValues.UserId + "} password";
            const string logAfter = "User's {" + LogValues.UserId + "} password was changed";

            _logger.LogDebug(logBefore, userId);
            await _origin.ChangePasswordAsync(userId, password);
            _logger.LogInformation(logAfter, userId);
        }

        public Task<IEnumerable<ClientModel>> GetClientsAdmissionsAsync(Guid userId)
            => _origin.GetClientsAdmissionsAsync(userId);

        public Task<int> GetCountAsync(string? filter)
            => _origin.GetCountAsync(filter);

        public Task<(IEnumerable<UserModel> List, int MaxCount)> GetListAsync(int count, int offset, string? filter)
            => _origin.GetListAsync(count, offset, filter);

        public Task<UserModel?> GetUserAsync(Guid id)
            => _origin.GetUserAsync(id);

        public async Task<Guid> InsertAsync(UserModel user)
        {
            const string logBefore = "Insertings user: {@data}";
            const string logAfter = "User with id={" + LogValues.UserId + "} was inserted: {@data}";

            _logger.LogDebug(logBefore, user);
            var result = await _origin.InsertAsync(user);
            _logger.LogInformation(logAfter, result, user);

            return result;
        }

        public async Task UpdateAsync(UserModel model)
        {
            const string logBefore = "Updating user with id={" + LogValues.UserId +"}: {@data}";
            const string logAfter = "User with id={" + LogValues.UserId + "} was updated with data: {@data}";

            _logger.LogDebug(logBefore, model.Id, model);
            await _origin.UpdateAsync(model);
            _logger.LogInformation(logAfter, model.Id, model);
        }
    }
}
