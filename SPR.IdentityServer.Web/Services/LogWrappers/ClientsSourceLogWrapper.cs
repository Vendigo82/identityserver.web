﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services.LogWrappers
{
    public class ClientsSourceLogWrapper<T> : IClientsSource where T : IClientsSource
    {
        private readonly IClientsSource _origin;
        private readonly ILogger<T> _logger;

        public ClientsSourceLogWrapper(T origin, ILogger<T> logger)
        {
            _origin = origin ?? throw new ArgumentNullException(nameof(origin));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public Task<ClientModel?> GetClientAsync(Guid id)
        {
            return _origin.GetClientAsync(id);
        }

        public Task<IEnumerable<ClientModel>> GetClientsAsync()
        {
            return _origin.GetClientsAsync();
        }

        public Task<JToken> GetConfigurationAsync(Guid id) => _origin.GetConfigurationAsync(id);

        public async Task<Guid> InsertAsync(ClientModel model)
        {
            _logger.LogDebug("Inserting client {ClientId} '{Client}': {@data}", model.Id, model.SystemName, model);
            var result = await _origin.InsertAsync(model);
            _logger.LogInformation("Client '{Client}' was inserted with id={ClientId}; data: {@data}", model.SystemName, result, model);
            return result;
        }

        public async Task UpdateAsync(ClientModel model)
        {
            _logger.LogDebug("Updating client {ClientId} '{Client}': {@data}", model.Id, model.SystemName, model);
            await _origin.UpdateAsync(model);
            _logger.LogInformation("Client {ClientId} '{Client}' was updated: {@data}", model.Id, model.SystemName, model);
        }

        public async Task UpdateConfigurationAsync(Guid id, JToken configuration)
        {
            await _origin.UpdateConfigurationAsync(id, configuration);
            _logger.LogInformation("Update configuration for client '{ClientId}': {Configuration}", id, configuration);
        }
    }
}
