﻿using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.Abstractions;
using System;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services
{
    public class ApiService : IApiService
    {
        private readonly ApiClient.IApiClient _client;

        public ApiService(IApiClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<string> HashSecretAsync(string secret)
        {
            var response = await _client.ApiTemplateHashAsync(secret);
            return response.Hash!;
        }
    }
}
