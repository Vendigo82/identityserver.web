﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services
{
    public class UsersSource : IUsersSource
    {
        private readonly IApiClient _client;
        private readonly IMapper _mapper;

        public UsersSource(IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<(IEnumerable<UserModel> List, int MaxCount)> GetListAsync(int count, int offset, string? filter)
        {
            var response = await _client.ApiUsersGetAsync(offset, count, filter);
            var result = _mapper.Map<IEnumerable<UserModel>>(response.Items);
            return (result, response.MaxCount);
        }

        public async Task<int> GetCountAsync(string? filter)
        {
            var response = await _client.ApiUsersCountAsync(filter);
            return response.Count;
        }

        public async Task<UserModel?> GetUserAsync(Guid id)
        {
            try {
                var item = await _client.ApiUsersGetAsync(id);
                return _mapper.Map<UserModel>(item);
            } catch (ApiException e) {
                if (e.StatusCode == StatusCodes.Status404NotFound)
                    return null;

                throw;
            }
        }

        public async Task<Guid> InsertAsync(UserModel user)
        {
            var request = _mapper.Map<UserDto>(user);
            try {
                var response = await _client.ApiUsersPutAsync(request);
                return response.Id;
            } catch (ApiException<ProblemDetails> e) { 
                if (e.StatusCode == StatusCodes.Status409Conflict)
                    throw new ConflictException($"User's login conflict: {user.Login}", e);
                else
                    throw;
            }
        }

        public async Task UpdateAsync(UserModel model)
        {
            var item = _mapper.Map<UserDto>(model);
            try {
                await _client.ApiUsersPostAsync(item);
            } catch (ApiException<ProblemDetails> e) {
                switch (e.StatusCode) {
                    case StatusCodes.Status404NotFound:
                        throw new ItemNotFoundException($"User with id={model.Id} does not found", e);
                    case StatusCodes.Status409Conflict:
                        throw new ConflictException($"Users's login conflict: {model.Login}", e);
                    default:
                        throw;
                }
            }
        }

        public async Task ChangePasswordAsync(Guid userId, string password)
        {
            try {
                await _client.ApiUsersPasswordAsync(userId, new ChangePasswordRequest { Password = password });
            } catch (ApiException e) {
                switch (e.StatusCode) {
                    case StatusCodes.Status404NotFound:
                        throw new ItemNotFoundException($"User with id={userId} does not found", e);
                    default:
                        throw;
                }
            }
        }

        public async Task<IEnumerable<ClientModel>> GetClientsAdmissionsAsync(Guid userId)
        {
            var clients = await _client.ApiUsersClientsGetAsync(userId);
            var list = _mapper.Map<IEnumerable<ClientModel>>(clients.Items);
            return list;
        }

        public async Task ChangeClientsAdmissionsAsync(Guid userId, IEnumerable<Guid> grant, IEnumerable<Guid> revoke)
        {
            await _client.ApiUsersClientsPostAsync(userId, new UpdateUserClientsRequest {
                Grant = grant.ToList(),
                Revoke = revoke.ToList()
            });
        }
    }
}
