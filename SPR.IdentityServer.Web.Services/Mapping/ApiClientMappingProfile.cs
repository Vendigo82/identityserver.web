﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.DataModel;

namespace SPR.IdentityServer.Web.Services.Mapping
{
    public class ApiClientMappingProfile : Profile
    {
        public ApiClientMappingProfile()
        {
            CreateMap<ClientDto, ClientModel>().ReverseMap();

            CreateMap<ApiClient.LoginTypes, DataModel.LoginTypes>()
                .ConvertUsingEnumMapping(o => o.MapByName())
                .ReverseMap();

            CreateMap<UserDto, UserModel>().ReverseMap();
        }
    }
}
