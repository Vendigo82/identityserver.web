﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using SPR.IdentityServer.ApiClient;
using SPR.IdentityServer.Web.Abstractions;
using SPR.IdentityServer.Web.DataModel;
using SPR.IdentityServer.Web.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.IdentityServer.Web.Services
{
    public class ClientsSource : IClientsSource
    {
        private readonly IApiClient _client;
        private readonly IMapper _mapper;

        public ClientsSource(IApiClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<ClientModel>> GetClientsAsync()
        {
            var list = await _client.ApiClientsGetAsync();
            return _mapper.Map<IEnumerable<ClientModel>>(list.Items);
        }


        public async Task<ClientModel?> GetClientAsync(Guid id)
        {
            try {
                var item = await _client.ApiClientsGetAsync(id);
                return _mapper.Map<ClientModel>(item);
            } catch (ApiException e) {
                if (e.StatusCode == StatusCodes.Status404NotFound)
                    return null;

                throw;
            }
        }

        public async Task<Guid> InsertAsync(ClientModel model)
        {
            var item = _mapper.Map<ClientDto>(model);
            try {
                var response = await _client.ApiClientsPutAsync(item);
                return response.Id;
            } catch (ApiException<ProblemDetails> e) {
                if (e.StatusCode == StatusCodes.Status409Conflict)
                    throw new ConflictException($"Client's name conflict: {model.SystemName}", e);
                else
                    throw;
            }
        }

        public async Task UpdateAsync(ClientModel model)
        {
            var item = _mapper.Map<ClientDto>(model);
            try {
                await _client.ApiClientsPostAsync(item);
            } catch (ApiException<ProblemDetails> e) {
                switch (e.StatusCode) {
                    case StatusCodes.Status404NotFound:
                        throw new ItemNotFoundException($"Client with id={model.Id} does not found", e);
                    case StatusCodes.Status409Conflict:
                        throw new ConflictException($"Client's name conflict: {model.SystemName}", e);
                    default:
                        throw;
                }
            }
        }

        public async Task<JToken> GetConfigurationAsync(Guid id)
        {
            try
            {
                var item = await _client.ApiClientsConfigurationGetAsync(id);
                return item;
            } catch (ApiException e) when (e.StatusCode == StatusCodes.Status204NoContent) {
                return JValue.CreateNull();
            }
        }

        public async Task UpdateConfigurationAsync(Guid id, JToken configuration)
        {
            await _client.ApiClientsConfigurationPostAsync(id, configuration);
        }
    }
}
